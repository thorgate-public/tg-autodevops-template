# Django Project template Auto DevOps template

**Note:** This is work in progress, correct releases will be tagged.

Usage

In `.gitlab-ci.yml` add:
```yaml
include:
- project: "thorgate-public/tg-autodevops-template"
  ref: master
  file: "/Auto-DevOps.gitlab-ci.yml"
```

## Configuration

| Environment variable                  | Description                                                                                                                                 | Default                                         |
|:--------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------|:------------------------------------------------|
| `AUTO_DEVOPS_APPLICATION_NAME`        | Auto DevOps application name, used as part of the release name for default chart.                                                           | `$CI_PROJECT_PATH_SLUG`                         |
| `AUTO_DEVOPS_COMPONENTS`              | Auto DevOps components to build and deploy separated with spaces                                                                            | `undefined`                                     |
| `AUTO_DEVOPS_ENVIRONMENT_FALLBACK`    | Auto DevOps Environment name override, if this is defined then this value is used instead of `$CI_ENVIRONMENT_SLUG`.                        | Default `undefined`, for review stage `staging` |
| `AUTO_DEVOPS_SCOPED_SETTINGS`         | Auto DevOps Environment variable configuration, if enabled all variables supplied to applications are expected to contain environment name. | `1`                                             |
| `AUTO_DEVOPS_CONFIGURATION_DIRECTORY` | Auto DevOps additional configuration scripts to create required files during testing or deployment.                                         | `.auto-devops`                                  |
| `AUTO_DEVOPS_SECRETS_PASSWORD`        | Auto DevOps secrets password.                                                                                                               | `undefined`                                     |
| `KUBERNETES_VERSION`                  | Kubernetes client version used for deployments. Only major and minor version is required.                                                   | `1.15`                                          |


## Application secrets

Application secrets are expected to be prefixed by `K8S_SECRET_${component}_` or `K8S_SECRET_${env_slug}_${component}_`, the later case is only used when `AUTO_DEVOPS_SCOPED_SETTINGS` is enabled.
When `AUTO_DEVOPS_SCOPED_SETTINGS` is enabled, then `K8S_SECRET_${env_slug}_${component}_` overrides anything that comes from `K8S_SECRET_${component}_`
Only settings for the matching component are added to deployment. Meaning if you have 2 components that should have separate values for the same key, they do not collide with each other.

As additional option there is support to use encrypted file from the project repository to expose those secrets during deploy phase.
To create secrets:
```shell script
cat <<EOF >"$AUTO_DEVOPS_CONFIGURATION_DIRECTORY/secrets.data"
STAGING_DJANGO_SECRET_KEY=something-really-secret
STAGING_DJANGO_DATABASE_PASSWORD=something
EOF

# Password is base64 encoded to prevent interpolation
openssl enc -a -aes-256-cbc -pbkdf2 -iter 20000 \
  -in "$AUTO_DEVOPS_CONFIGURATION_DIRECTORY/secrets.data" \
  -out "$AUTO_DEVOPS_CONFIGURATION_DIRECTORY/secrets.enc" \
  -k "$(echo "TEST_\$PASSWORD" | base64)"
```

## Build phase

**Requirements:**

- Defined value for `AUTO_DEVOPS_COMPONENTS`
  - Example: `AUTO_DEVOPS_COMPONENTS: "django node"`


## Testing phase

Testing phase follows similar process as [Thorgate project template](https://gitlab.com/thorgate-public/django-project-template).
This means that `docker-compose` is used for testing phase with one of exceptions, instead of re-building the docker images it uses images built in previous stage.

**Requirements:**

- Project root must contain:
  - `$AUTO_DEVOPS_CONFIGURATION_DIRECTORY` directory with executable script `setup-tests.sh` - this file is used to create testing
  - `Makefile` with directives (supported by [Thorgate project template](https://gitlab.com/thorgate-public/django-project-template)):
    - `docker-compose` - To call docker-compose commands while accepting `$DOCKER_COMPOSE_OVERRIDE` as override file from the environment
    - `settings` - To create required setup for the project
    - `quality` - To run all code quality tools for this project
    - `coverage` - To run all testing tools for this project and generate code coverage report


## Deploy phase

**Requirements:**

TODO
